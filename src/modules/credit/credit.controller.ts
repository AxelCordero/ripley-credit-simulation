import { Body, Controller, Post } from '@nestjs/common';
import { CreditService } from './credit.service';
import { SimulationRequestData } from './credit.interface';

@Controller('credit')
export class CreditController {
  constructor(private readonly creditService: CreditService) {}

  @Post('simulate')
  simulate(@Body() simulationRequestData: SimulationRequestData) {
    return this.creditService.simulateCredit(simulationRequestData);
  }
}
