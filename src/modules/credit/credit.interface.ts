export interface SimulationRequestData {
  RutCliente: string;
  MontoDelCredito: number;
  NumeroCuotas: number;
}
