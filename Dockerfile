# Imagen base
FROM node:18

# Directorio de trabajo
WORKDIR /usr/src/app

# Copiar package.json y package-lock.json
COPY package*.json ./

# Instalar dependencias
RUN npm install 

# Copiar el resto de los archivos del proyecto
COPY . .

# Iniciar la aplicación en modo de desarrollo
CMD ["npm", "run", "start:dev"]