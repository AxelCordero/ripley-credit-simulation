import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CreditModule } from './modules/credit/credit.module';

@Module({
  imports: [CreditModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
