import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { SimulationRequestData } from './credit.interface';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class CreditService {
  constructor(private httpService: HttpService) {}

  async simulateCredit(simulationRequestData: SimulationRequestData) {
    try {
      const logData: any = {};

      const rateValue = await this.getRate();
      const clientRut = await this.getClientData(
        simulationRequestData.RutCliente,
      );

      logData['Request'] = {
        RutCliente: clientRut.rut,
        MontoDelCredito: simulationRequestData.MontoDelCredito,
        NumeroCuotas: simulationRequestData.NumeroCuotas,
      };
      const valorCuota =
        (+simulationRequestData.MontoDelCredito /
          +simulationRequestData.NumeroCuotas) *
        +rateValue;
      const response = {
        status: HttpStatus.OK,
        ValorCuota: Math.round(valorCuota),
      };

      logData['Response'] = {
        ...response,
      };
      await this.saveLog({ json_TRX: JSON.stringify(logData) });
      return {
        response,
      };
    } catch (error) {
      console.log(error);
      throw new BadRequestException('An error occurred while simulating');
    }
  }

  async getClientData(rut: string) {
    const url = `https://8a8axu9k40.execute-api.us-east-1.amazonaws.com/desa/getcliente?rut=${rut}`;
    try {
      const response = await this.httpService.get(url).toPromise();
      return response.data;
    } catch (error) {
      return { rut: '1-9' };
    }
  }

  async getRate() {
    const url = `https://8a8axu9k40.execute-api.us-east-1.amazonaws.com/desa/gettasa?`;
    try {
      const response = await this.httpService.get(url).toPromise();
      return response.data;
    } catch (error) {
      return 3.0;
    }
  }

  async saveLog(trxData) {
    const url = `${process.env.URL_RIPLEY_LOG}/save-log`;
    console.log('url ????? ', url);
    try {
      const response = await this.httpService.post(url, trxData).toPromise();
      return response.data;
    } catch (error) {
      throw new BadRequestException('An error occurred saving trx_log');
    }
  }
}
